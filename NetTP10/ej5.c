#include <stdio.h>
#include <stdint.h>
#include "ej5.h"
#define ERROR -1                                            //efinimos una constante para error

int bits(int num);                                          //dependiendo del bit a modificar se obtendera una mascara acorde

typedef struct                                              //definimos una estructura para los puertos A y B
{
    char a;
    char b;
}PORT_AB;

typedef union                                               //estructura que une el puerto AB con el puerto D
{
    uint16_t port_d;
    PORT_AB port_ab;
}PORT_D;

static PORT_D ports;                                        //indicamos que la variable sea estatica y solo se pueda modificar por este archivo o llamados a funciones del mismo

void bitset(char port, int num)                             //funcion que cambia el estado del bit indicado a 1
{
    num = bits(num);                                        //llamamos a la funcion bits para obtener la mascara deseada
    if(num != ERROR)                                           //si el numero del bit a modificar es valido se modifica el puerto deseado
    {
        if(port == 'A')
        {
            ports.port_ab.a  = (ports.port_ab.a | num);     //cambiamos el bit deseado a 1
        }
        else if(port == 'B')
        {
            ports.port_ab.b  = (ports.port_ab.b | num);
        }
        else if(port == 'D')
        {
            ports.port_d  = (ports.port_d | num);
        }
        else
        {
            printf("Error: mal ingresado el puerto");       //si el puerto esta mal indicado se imprime un error
        }
    }
    else
    {
        printf("ERROR: mal ingrsado el numero de bit");     //si el bit a modificar se ingreso mal se imprime un error
    }
}

void bitclr(char port, int num)                             //funcion que cambia el estado del bit indicado a 0
{
    num = bits(num);                                        //llamamos a la funcion bits para obtener la mascara deseada
    if(num != ERROR)                                           //si el numero del bit a modificar es valido se modifica el puerto deseado
    {
        if(port == 'A')
        {
            ports.port_ab.a  = (ports.port_ab.a & (~num));  //cambiamos el bit deseado a 0
        }
        else if(port == 'B')
        {
            ports.port_ab.b  = (ports.port_ab.b & (~num));
        }
        else if(port == 'D')
        {
            ports.port_d  = (ports.port_d & (~num));
        }
        else
        {
            printf("Error: mal ingresado el puerto");       //si el puerto esta mal indicado se imprime un error
        }
    }
    else
    {
        printf("ERROR: mal ingrsado el numero de bit");     //si el bit a modificar se ingreso mal se imprime un error
    }
}

int bitget(char port, int num)                              //funcion que devuelve le valor del bit indicado
{
    int valor;
    num = bits(num);                                        //llamamos a la funcion bits para obtener la mascara deseada
    if(num != ERROR)                                           //si el numero del bit a modificar es valido se modifica el puerto deseado
    {
        if(port == 'A')
        {
            valor  = (ports.port_ab.a & num);               //guardamos en valor el resultado de aplicarle la mascara
        }
        else if(port == 'B')
        {
            valor  = (ports.port_ab.b & num);
        }
        else if(port == 'D')
        {
            valor  = (ports.port_d & num);
        }
        else
        {
            printf("Error: mal ingresado el puerto");       //si el puerto esta mal indicado se imprime un error
        }
        if(valor != 0)                                      //si valor es diferente de 0 quiere decir que el bit que nos interesa es 1 de lo contrario su valor es 0
        {
            valor = 1;
        }
    }
    else
    {
        printf("ERROR: mal ingrsado el numero de bit");     //si el bit a obtener se ingreso mal se imprime un error
    }
    return valor;                                           //devolvemos el valor del bit 
}

void bittoggle(char port, int num)                          //funcion que cambia el estado del bit indicado a su opuesto
{
    num = bits(num);                                        //llamamos a la funcion bits para obtener la mascara deseada
    if(num != ERROR)                                           //si el numero del bit a modificar es valido se modifica el puerto deseado
    {
        if(port == 'A')
        {
            ports.port_ab.a  = (ports.port_ab.a ^ num);     //cambiamos el estado del bit deseado
        }
        else if(port == 'B')
        {
            ports.port_ab.b  = (ports.port_ab.b ^ num);
        }
        else if(port == 'D')
        {
            ports.port_d  = (ports.port_d ^ num);
        }
        else
        {
            printf("Error: mal ingresado el puerto");       //si el puerto esta mal indicado se imprime un error
        }
    }
    else
    {
        printf("ERROR: mal ingrsado el numero de bit");     //si el bit a modificar se ingreso mal se imprime un error
    }
}

void maskon(char port, int mask)                            //funcion que prende todos los bits indicados en la mascara
{
    if(port == 'A')
    {
        ports.port_ab.a = (ports.port_ab.a | mask);         //dependiendo de la mascara se encienden los bits
    }
    else if(port == 'B')
    {
        ports.port_ab.b = (ports.port_ab.b | mask);
    }
    else if(port == 'D')
    {
        ports.port_d = (ports.port_d | mask);
    }
    else
    {
        printf("Error: mal ingresado el puerto");           //si el puerto esta mal indicado se imprime un error
    }
}

void maskoff(char port, int mask)                           //funcion que apaga todos los bits indicado es la mascara
{
    if(port == 'A')
    {
        ports.port_ab.a = ~(~(ports.port_ab.a) | mask);     //dependiendo de la mascara se apagan los bits
    }
    else if(port == 'B')
    {
        ports.port_ab.b = ~(~(ports.port_ab.b) | mask);
    }
    else if(port == 'D')
    {
        ports.port_d = ~(~(ports.port_d) | mask);
    }
    else
    {
        printf("Error: mal ingresado el puerto");           //si el puerto esta mal indicado se imprime un error
    }
}

void masktoggle(char port, int mask)                        //funcion que cambia el estado de todos los bits indicados en la mascara al opuesto
{
    if(port == 'A')
    {
        ports.port_ab.a = (ports.port_ab.a ^ mask);         //dependiendo de la mascara se cambia el estado de los bits
    }
    else if(port == 'B')
    {
        ports.port_ab.b = (ports.port_ab.b ^ mask);
    }
    else if(port == 'D')
    {
        ports.port_d = (ports.port_d ^ mask);
    }
    else
    {
        printf("Error: mal ingresado el puerto");           //si el puerto esta mal indicado se imprime un error
    }
}

int bits(int num)                                           //dependiendo del bit a modificar se obtendera una mascara acorde
{
    switch (num)                                            //mascaras correspondientes a cada bit de los puertos
    {
        case 0:
            num = 0x0001;
            break;
        case 1:
            num = 0x0002;
            break;
        case 2:
            num = 0x0004;
            break;
        case 3:
            num = 0x0008;
            break;
        case 4:
            num = 0x0010;
            break;
        case 5:
            num = 0x0020;
            break;
        case 6:
            num = 0x0040;
            break;
        case 7:
            num = 0x0080;
            break;
        case 8:
            num = 0x0100;
            break;
        case 9:
            num = 0x0200;
            break;
        case 10:
            num = 0x0400;
            break;
        case 11:
            num = 0x0800;
            break;
        case 12:
            num = 0x1000;
            break;
        case 13:
            num = 0x2000;
            break;
        case 14:
            num = 0x4000;
            break;
        case 15:
            num = 0x8000;
            break;
        default:
            num = ERROR;                                    //si el bit a modificar no corresponde a 2bytes num toma el valor de -1 y se interpreta como error
    }
    return num;                                             //devolvemos la mascara
    
}