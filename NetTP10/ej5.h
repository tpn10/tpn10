#ifndef EJ5_H
#define EJ5_H

void bitset(char port, int num);                //funcion que cambia el estado del bit indicado a 1
void bitclr(char port, int num);                //funcion que cambia el estado del bit indicado a 0
int bitget(char port, int num);                 //funcion que devuelve le valor del bit indicado
void bittoggle(char port, int num);             //funcion que cambia el estado del bit indicado a su opuesto
void maskon(char port, int mask);               //funcion que prende todos los bits indicados en la mascara
void maskoff(char port, int mask);              //funcion que apaga todos los bits indicado es la mascara
void masktoggle(char port, int mask);           //funcion que cambia el estado de todos los bits indicados en la mascara al opuesto

#endif /* EJ5_H */

