#include <stdio.h>
#include <stdlib.h>
#include "ej5.h"                                                //libreria usada para las funciones principales del programa
#define ERROR -1                                                //constante ERROR
#define port 'A'                                                //constante port A

int leer(int *quitflag);                                        //funcion que interpreta el valor ingresado
void imprime(void);                                             //funcion que imprime los LEDs conectados al puerto A

int main(void)
{
    int quitflag = 0;                                           //flag que termina el programa al activarse
    int valor;                                                  //variable que contiene el valor ingresado
    do
    {
       valor = leer(&quitflag);                                 //llamamos a la funcion leer y guardamos en valor
       if(valor != ERROR)                                       //verificamos si hay error
        {
            if(quitflag == 0)                                   //verificamos que no se haya presionado q
            {
                imprime();                                      //imprimimos los LEDs conectados al puerto A
            }
       }
       else
       {
           printf("ERROR\n");                                   //si hay error se imprime
       }
    }
    while(quitflag == 0);                                       //cuando se presiona q quitflag toma el valor 1 para indicar que se debe finalizar
    
    return 0;
}

int leer(int *quitflag)                                         //funcion que interpreta el valor ingresado
{
    int valor = 0;
    int x;
    int cont = 0;                                               //contador de numeros ingresados
    int mask = 0xFF;                                            //mascara que indicara que se deben modificar todos los bits
    
    while(((x = getchar()) != '\n') || (cont == 0))             //mientra no se presione enter o el contador de numeros sea igual a 0
    {
        if((x >= '0') && (x <= '7'))                            //lee si se quiere nodificar algun bit del 0 al 7
        {
            valor = (x-'0');                                    //convertimos a decimal el ascii
            bitset(port, valor);                                //llamamos a la libreria de bitset que activa el bit seleccionado
        }
        else if(x == 't')                                       //si presiona t
        {
            masktoggle(port, mask);                             //llamamos a la libreria masktoggle que intercambia el estado de todos los bits
        }
        else if(x == 'c')                                       //si se presiona c
        {
            maskoff(port, mask);                                //llamamos a la libreria de maskoff para apagar todos los bits
        }
        else if(x == 's')                                       //si se presiona s
        {
            maskon(port, mask);                                 //llamamos a la libreria de maskon para encender todos los bits
        }
        else if(x == 'q')                                       //si se presiona q
        {
            *quitflag = 1;                                      //por medio de un puntero se activa el flag
        }
        else                                                    //si se ingreso cualquier otro valor
        {
            valor = ERROR;                                      //se guarda la constante ERROR en valor
        }
        cont++;                                                 //incrementamos el contador de caracteres ingresados, SI SE INGRESA MAS DE UN CARACTER SOLO SE LEERA EL PRIMERO Y E PROGRAMA FUNCIONARA EN CONSECUENCIA
    }
    return valor;                                               //devolvemos el valor
}

void imprime(void)                                              //funcion que imprime los LEDs conectados al puerto A
{
    int i;
    printf("LEDs Port A: ");
    for(i = 7; i >= 0; i--)
    {
        printf("%d", bitget(port, i));                          //imprimimos el estado de todos los bits del puerto A
    }
    printf("\n");
}

